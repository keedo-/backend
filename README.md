# Keedo Backend

## Summary

This is the backend application of [Keedo](https://keedo.tn). 

The [frontend web application](https://gitlab.com/keedo-/web-front) 

## Features

* TODO

## Status

* In progress

## Requirements
 
platform : Nodejs & mongodb 

## run 

### 1. setup env vars 

`DEBUG=*;`, optional : used to configure [debug](https://github.com/visionmedia/debug/)

`KEEDO_NODE_ENV=production` , Environement, `development`, `staging`, `production`

`KEEDO_SMTP_URI=smtp://<smtp_uri>`

`KEEDO_ALLOWED_ORIGINS=`, A `,` separated list to define allowed origins (example : `http://localhost:8081,https://keedo.tn`)

`KEEDO_MONGODB_URL=mongodb://<mongdb_db_uri>`, Mongodb database uri 

`KEEDO_UPLOAD_FOLDER=<upload_folder>`, Path to media folder for uploading and serving file

`KEEDO_REDIS_HOST=<redis_db_uri>` Redis uri

`KEEDO_ASSETS_HOST=Assets host`, path to assets host, if you are using nginx or any other server for assets

`KEEDO_APP_DEFAULT_PORT=8001;`, default app port

`KEEDO_ADMIN_EMAIL=<the admin's email>`, used to send some types of emails to admin 

### 

run `node bin\www`



## demo gif 

TODO 

## License

MIT



