// content of index.js
const http = require('http')
const port = process.env.KEEDO_BACKEND_PORT;

const requestHandler = (request, response) => {
    console.log(request.url);
    response.end('Hello Node.js Server!')
}

// 6379
let url = process.env.KEEDO_REDIS + ':' + '6379/ping';
console.log(url);
http.get(url, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
        data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
        console.log(JSON.parse(data).explanation);
    });

}).on("error", (err) => {
    console.log(err);
    console.log("Error: " + err.message);
});

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})
