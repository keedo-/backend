#!/usr/bin/env bash
docker run  \
	-d \
	--name "example_api" \
	-p $port:port \
	-e "APP=index.js" \
	-e "NODE_ENV=development" \
	-v $(pwd)/api:/var/www/example.com/api \
	example/nodejs;