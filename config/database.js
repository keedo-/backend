let debug = require("debug")("config:database");
debug(process.env.KEEDO_NODE_ENV);
let config = {
	development: {
		dbUrl: 'localhost'
	},
	production: {
		dbUrl: process.env.KEEDO_MONGODB_URL
	}
};
config.env = process.env.KEEDO_NODE_ENV || 'development';

module.exports = config;

