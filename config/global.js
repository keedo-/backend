let baseConfig = {
	uploadFolder: process.env.KEEDO_UPLOAD_FOLDER,
	redis: process.env.KEEDO_REDIS_HOST,
	assetsHost: process.env.KEEDO_ASSETS_HOST,
	adminEmail: process.env.KEEDO_ADMIN_EMAIL
}
let config = {
	development: Object.assign({}, baseConfig, {
		host: 'localhost:8081', // change this to put your own db
	}),
	staging: Object.assign({}, baseConfig, {
		host: 'https://www.keedo.tn',
	}),
	production: Object.assign({}, baseConfig, {
		host: 'https://www.keedo.tn',
	})
};
config.env = process.env.KEEDO_NODE_ENV || 'development';

module.exports = config;

