// server.js

// set up ======================================================================
// get all the tools we need
let express = require('express');
let path = require('path');
let i18n = require('./i18n');


let debug = require('debug')("app:index");
//
let app = express();
let flash = require('connect-flash');

let morgan = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let session = require('express-session');
let RedisStore = require('connect-redis')(session);


let allowedOrigins = process.env.KEEDO_ALLOWED_ORIGINS.split(',');

let config = require('./../config/global');

// Add headers
app.use(function (req, res, next) {
	// Website you wish to allow to connect
	let origin = req.headers.origin;
	if (allowedOrigins.indexOf(origin) > -1) {
		debug("allowing for : ", origin);
		res.setHeader('Access-Control-Allow-Origin', origin);
	}

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});


// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());


// required for passport
// TODO , add ssl
app.use(session({
	secret: 'gotothehell',
	resave: false,
	saveUninitialized: false,
	secure: true,
	//store: new RedisStore({host: config[config.env].redis, ttl: 60 * 60 * 24 * 30 * 100})
})); // session secret
app.use(flash()); // use connect-flash for flash messages stored in session

// passport
let passport = require('./passport/init'); // pass passport for configuration
app.use(passport.initialize());
app.use(passport.session());


app.use('/assets', express.static(path.join(__dirname, 'assets')));


// VIEWS =====================================
// locales
app.use(function (req, res, next) {
	// express helper for natively supported engines
	res.locals.__ = res.__ = function (key, options) {
		return i18n.t.apply(this, [req.locale || 'fr'].concat([key, options]));
	};
	next();
});


// Views and routes
app.set('view engine', 'pug');
app.engine('pug', require('pug').__express);
app.set('views', path.join(__dirname, 'views'));
app.disable('view cache')

app.use(require('./routes/index'));


// API routes ======================================================================
app.use('/api', require('./routes/api/index.js')(passport)); // load our routes and pass in our app and fully configured passport
//
app.use(function (req, res) {
	debug('request');
	res.send({ok: false})
});
module.exports = app;
