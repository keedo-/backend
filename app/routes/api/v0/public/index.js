let express = require('express');
let router = express.Router();
let debug = require('debug')('public :route');

router.use('/schools', require('./schools'));
router.use('/profile', require('./profile'));
router.use('/classes', require('./classes'));
router.use('/token', require('./token'));

module.exports = router;
