let express = require('express');
let router = express.Router();
let debug = require('debug')("api:v0:schools");


let School = require('../../../../models/school');
let Class = require('../../../../models/class');

const log = require('../../../../utils/log');

/**
 *
 * @param req
 * @param res
 * @param next
 */
function loggedIn(req, res, next) {
	debug(req.user);
	if (req.user) {
		next();
	} else {
		res.status(401).send({status: 401, reason: "not_authorized"});
	}
}

/**
 * Get list of brands based on geo
 */
router.get('/', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	School
			.find(
					{
						"name":
								{$regex: new RegExp("^" + search.toLowerCase(), "i")}
					})
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				log('error:schools/', err);
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send({ok: true, data: results});
			});
});

/**
 * Get list of brands based on geo
 */
router.get('/:id/products', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	Class
			.find(
					{
						"brand": req.params.id,
						"name":
								{$regex: new RegExp("^" + search.toLowerCase(), "i")}
					})
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				log('schools/:id/products', err);
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send(results);
			});


});

/**
 * Get list of brands based on geo
 */
router.get('/:id/students', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	School
			.find(
					{
						"id": req.params.id
					})
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send(results);
			});


});

router.post('/:id/follow', loggedIn, function (req, res) {

});

module.exports = router;
