let express = require('express')
let router = express.Router();
let Class = require('../../../../models/class');

router.get('/', function (req, res) {
	Class.find({
		students: {$in: [req.user.id]}
	}, (err, results) => {
		res.send({ok: true, data: results});
	});

});

module.exports = router;
