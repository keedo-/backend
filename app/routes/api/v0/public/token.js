let express = require('express');
let debug = require('debug')('app:routes:api:v0:public:token');


let Token = require('../../../../models/token');
let router = express.Router();
router.post('/register', function (req, res) {
    let body = req.body;
    let token = body.token;
    let oldToken = body.oldToken;
    Token.findOne({token: oldToken}, function (err, result) {
        if (err) {
            return res.status(500).send({ok: false, reason: 'server'});
        }
        // if no result create it
        if (!result) {
            debug('new token to be created ');
            result = new Token({
                provider: body.provider,
                apiToken: body.apiToken
            });
        }
        else {
            debug('Token with this value exists , update it then ')
        }
        result.token = token;
        result.save(function (err, resultSaving) {
            if (err) {
                return res.status(500).send({ok: false, reason: 'server'});
            }

            debug('sucess update or create token ');
            res.send({ok: true, data: resultSaving});
        })

    });
});
/**
 *
 */
router.post('/update-position', function (req, res) {
    let body = req.body;
    let position = body.position;
    let token = body.token;
    Token.findOne({token: token}, function (err, result) {
        if (err) {
            debug("************error finding token*************");
            return res.status(500).send({ok: false, reason: 'server'});
        }
        if (!result) {
            debug('OOPs something went wrong, no token found  for geo update');
            return res.status(404).send({ok: false, reason: 'not_found'});
        }
        debug("************Find token *************");
        let coordinates = [parseFloat(String(position.coords.latitude)), parseFloat(String(position.coords.longitude))];
        debug("coordinates ");
        debug(position);
        result.location = {
            type: "Point",
            coordinates: coordinates,
            full: position
        };
        result.validate(function (err) {
            if (err) {
                debug("************error validation *************");
                debug(err);
                return res.status(500).send({ok: false, reason: 'server'});
            }
            result.save(function (err, resultSaving) {
                if (err) {
                    debug("************error saving position *************");
                    debug(err);
                    return res.status(500).send({ok: false, reason: 'server'});
                }
                debug('sucess update geo token ');
                res.send({ok: true, data: resultSaving});
            })
        })
    })
    // try to find old token

});

module.exports = router;