let express = require('express');
let router = express.Router();

let Account = require('../../../../models/account');

router.get('/', function (req, res) {
	res.send({ok: true, data: req.user})
});
router.post('/', function (req, res) {
	Account.findById(req.user.id, function (err, user) {
		user.profile = req.body.profile;
		user.save().then((err, resutl) => {
			res.send({ok: true})
		})
	});
});

module.exports = router;
