let express = require('express');
let router = express.Router();
let debug = require('debug')('routes:api:v0:business:classes');

let TeacherRequest = require("../../../../models/teacherRequest");
let SchoolRequest = require("../../../../models/schoolRequest");

const emails = require('../../../../jobs/emails');


let config = require('../../../../../config/global');
/**
 *
 * @param requestType
 * @param email
 */
const sendAdminEmail = function (requestType, email) {
	emails.push({
		to: config[config.env].adminEmail,
		from: "no-reply@keedo.tn",
		subject: "request " + requestType,
		text: "Someone asks for " + email
	});
};
/**
 * Create brands for a business account
 */
router.post('/request-teacher-profile', function (req, res) {
	let data = req.body;
	if (!data.school) {
		return res.status(400).send({ok: false, reason: 'no school'});
	}
	TeacherRequest.findOne({state: 'WAITING', teacher: req.user.id}, (err, found) => {
		if (err) {
			return res.status(500).send({ok: false, reason: 'server'});
		}
		if (found) {
			return res.status(501).send({ok: false, reason: 'already exists'});
		}
		let entry = new TeacherRequest();
		entry.school = data.school;
		entry.teacher = req.user.id;
		entry.save(function (err, request) {
			if (err) {
				return res.status(500).send({ok: false, reason: err});
			}
			if (request) {
				sendAdminEmail('Teacher', req.user.email);
				return res.send({ok: true});
			} else {
				return res.send({ok: false, product: null});
			}
		});
	})

})
;
/**
 * Create brands for a business account
 */
router.post('/request-school-profile', function (req, res) {
	let data = req.body;
	if (!data.school) {
		return res.status(400).send({ok: false, reason: 'no school'});
	}
	SchoolRequest.findOne({state: 'WAITING', teacher: req.user.id}, (err, found) => {
		if (err) {
			return res.status(500).send({ok: false, reason: 'server'});
		}
		if (found) {
			return res.status(501).send({ok: false, reason: 'already exists'});
		}
		let entry = new SchoolRequest();
		entry.school = data.school;
		entry.createdBy = req.user.id;
		entry.address = data.address;
		entry.save(function (err, request) {
			if (err) {
				return res.status(500).send({ok: false, reason: err});
			}
			if (request) {
				sendAdminEmail('School', req.user.email);
				return res.send({ok: true});

			} else {
				return res.send({ok: false, product: null});
			}
		});
	})

})
;
module.exports = router;
