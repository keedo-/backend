let express = require('express');
let router = express.Router({mergeParams: true});
let debug = require('debug')('business:route');
router.use('/schools', require('./schools'));
router.use('/profile', require('./profile'));
router.use('/classes', require('./class/classes'));
module.exports = router;
