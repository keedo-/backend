let express = require('express');
let router = express.Router();
let debug = require('debug')('routes:api:v0:business:classes');


let Schools = require("../../../../models/school");


/**
 * Get brands for this business account
 */
router.get('/mine', function (req, res) {
	Schools.findOne({teachers: {$in: [req.user.id]}}, function (err, list) {
		if (err) {
			return res.status(500).send({ok: false, reason: 'server'});
		}
		return res.send({ok: true, data: list ? list.toJSON() : []});
	});
});

module.exports = router;
