let express = require('express');
let router = express.Router({mergeParams: true});

let debug = require('debug')('routes:api:v0:business:classes');
let Class = require("../../../../../models/class");
let School = require("../../../../../models/school");

let list = (id, req, res) => {
	// get brands of current user
	// we suppose that we
	Class.find({teacher: req.user.id/*, school: req.body.school*/}, (err, results) => {
		if (err) {
			return res.status(500).send({ok: false, reason: "server"});
		}
		return res.send({ok: true, data: results});
	});
};
/**
 * list of class
 */
router.get('/', list.bind(this, null));

/**
 * list of class
 */
router.get('/:id', function (req, res) {
	Class.findOne({_id: req.params.id, teacher: req.user.id/*, school: req.body.school*/}, (err, found) => {
		if (err) {
			return res.status(500).send({ok: false, reason: "server"});
		}
		if (found) {
			return res.send({ok: true, data: found});
		}
		res.send(404)

	});
});
/**
 * Create new class
 */
router.post('/', function (req, res) {
	let classItem = new Class();
	let body = req.body;
	classItem.name = body.name;
	classItem.description = body.description;
	classItem.school = body.school;

	classItem.teacher = req.user.id;

	if (classItem.school) {
		School.findById(req.school.id, (err, found) => {
			if (found) {
				classItem.save(function (err, product) {
					if (err) {
						return res.status(500).send({ok: false, reason: err});
					}
					if (product) {
						return res.send({ok: true, product: product});
					} else {
						return res.send({ok: false, product: null});
					}
				});
			}
			return res.send({ok: false, message: 'no school found'});
		})
	} else {
		classItem.save(function (err, classCreated) {
			if (err) {
				return res.status(500).send({ok: false, reason: err});
			}
			if (classCreated) {
				return res.send({ok: true, data: classCreated});
			} else {
				return res.send({ok: false});
			}
		});
	}


});
// require check is owner

router.use('/:id/', require('./gards'));
/**
 * Chapters
 */
router.use('/:id/chapters/', require('./chapters'));
/**
 * Create brands for a business account
 */
router.use('/:id/students/', require('./students'));

module.exports = router;
