let express = require('express');
let router = express.Router();

let debug = require('debug')('routes:api:v0:business:classes:gards');
let Class = require("../../../../../models/class");

module.exports = (req, res, next) => {
	let id = req.params.id;
	Class.findOne({teacher: req.user.id, _id: req.params.id}, (err, classInstance) => {
		if (err) {
			return res.status(500).send({ok: false, reason: 'server'})
		}
		if (!classInstance) {
			return res.status(404).send({ok: false})
		}
		// save the class instance
		//req.classInstanceObjecet = classInstance;
		next();
	});
};
