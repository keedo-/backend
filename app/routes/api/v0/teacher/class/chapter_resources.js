let express = require('express');
let router = express.Router({mergeParams: true});
let uuid = require('uuid');
let path = require('path');
let fs = require('fs');
let Busboy = require('busboy');
let debug = require('debug')('routes:api:v0:business:classes');


let helpers = require('../../../../../utils/helpers');

let uploadFolder = helpers.getUploadFolder();
let Chapter = require("../../../../../models/chapter");
let ChapterResource = require("../../../../../models/chapterResource");

/**
 * Create brands for a business account
 */
router.post('/upload', function (req, res) {
	let chapterId = req.params.chapter_id;
	let busboy = new Busboy({headers: req.headers});
	let pictures = [];
	busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
		Chapter.findOne({id: chapterId, teacher: req.user.id}, (err, chapter) => {
			if (chapter) {
				// if need upload
				let newFileName = req.params.id + '.' + uuid.v1() + '.' + filename.split('.').pop();
				let saveTo = path.join(uploadFolder, newFileName);
				debug('Uploading File : ' + saveTo);
				file.pipe(fs.createWriteStream(saveTo));
				file.on('end', function () {
					debug("end downloading file");
					pictures.push({
						filename: filename,
						chapter: chapterId,
						location: newFileName,
						storageLocation: uploadFolder
					});
				});
			}
		})
	});
	busboy.on('finish', function () {
		let collection = [];
		pictures.forEach((entry) => {
			collection.push(new ChapterResource(entry));

		});
		ChapterResource.collection.insert(collection, function (err, response) {
			if (err) {
				return res.status(500).send({ok: false, reason: 'server'})
			}
			return res.send({ok: true});
		})

	});
	req.pipe(busboy);
});
/**
 * Get brands for this business account
 */
router.get('/', function (req, res) {
	let chapterId = req.params.chapter_id;
	Chapter.findOne({id: chapterId, teacher: req.user.id}, (err, chapter) => {
		if (chapter) {
			ChapterResource.find({chapter: chapter._id}, function (err, resources) {
				if (err) {
					res.status(500).send({ok: false, reason: 'server'})
				}
				let list = [];
				let current;
				resources.map(entry => {
					// hide storage location
					current = entry.toJSON();
					delete current.storageLocation;
					current.location = current.location.replace(uploadFolder, '');
					if (current.location.startsWith("\\")) {
						current.location = entry.location.substr(1);
					}
					current.src = [helpers.getAssetsHost(), current.location].join('/');
					list.push(current);
				});
				return res.send({ok: true, list: list})
			})
		} else {
			return res.status(404).send({ok: false})
		}
	})
});


module.exports = router;
