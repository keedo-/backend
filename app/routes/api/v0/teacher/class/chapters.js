let express = require('express');
let router = express.Router({mergeParams: true});
let ChapterClass = require("../../../../../models/chapter");
/**
 * Create new class
 */
router.post('/', function (req, res) {
	let item = new ChapterClass();
	item.class = req.params.id;

	let body = req.body;
	item.description = body.description;
	item.name = body.description;
	item.teacher = req.user.id;

	item.save(function (err, chapterCreated) {
		if (err) {
			return res.status(500).send({ok: false, reason: err});
		}
		if (chapterCreated) {
			return res.send({ok: true, data: chapterCreated});
		} else {
			return res.send({ok: false});
		}
	});
});
/**
 * Chapters
 */
router.use('/:chapter_id/chapters/', require('./chapter_resources'));

module.exports = router;
