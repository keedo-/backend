let express = require('express');
let router = express.Router({mergeParams: true});

let studentsEmails = require('../../../../../emails/students');

let Account = require('../../../../../models/account');
let Class = require('../../../../../models/class');
let config = require('../../../../../../config/global');

/**
 *
 * @param locale
 * @param idClass
 * @param list
 */
function pushEmailInvitations(locale, idClass, list) {
	function pushEmailInvitation(locale, email) {
		let link = [config[config.env].host, 'dashboard', 'classes', idClass].join('/');
		studentsEmails.sendInvitationToClass(locale, {link, email});
	}

	list.forEach((entry) => {
		pushEmailInvitation(locale || 'fr', entry);
	})
}

/**
 * Create new class
 */
router.get('/search', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	Account
			.find(
					{
						"email":
								{$regex: new RegExp("^" + search.toLowerCase(), "i")},
						"accounType": "USER",
					})
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				console.log(err);
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send({ok: true, data: results});
			});
});


/**
 * Create new class
 */
router.post('/invite', function (req, res) {
	Class.findOne({teacher: req.user.id, _id: req.params.id}, (err, found) => {
		if (err) {
			return res.status(500).send({ok: false, reason: 'server'})
		}
		if (!found) {
			return res.status(404).send({ok: false})
		}

		let list = req.body.list;
		Account.find({email: {"$in": list}}, (err, results) => {
			if (err) {
				return res.status(500).send({ok: false, raison: 'server'})
			}
			const missedList = JSON.parse(JSON.stringify(list));
			const toInvite = [];
			let newStudents = [];
			if (results.length) {
				results.forEach(entry => {
					if (list.indexOf(entry.email) > -1) {
						toInvite.push(entry.email);
						newStudents.push(entry);
						missedList.filter(e => e !== entry.email);
					}
				});
				found.students = (found.students || []).concat(newStudents);
				found.save(function (err, result) {
					if (err) {
						return res.send(500).send({ok: false, reason: 'server'})
					}
					pushEmailInvitations(req.locale || 'fr', req.params.id, toInvite);
					res.send({ok: true, data: {missed: missedList, invited: toInvite}});
				})
			} else {
				res.send({ok: true, data: {missed: missedList, invited: toInvite}});

			}
		});

	});
})
;


module.exports = router;
