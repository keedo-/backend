let express = require('express');
let router = express.Router();
let debug = require('debug')('business:route');
router.use('/schools', require('./schools'));
router.use('/accounts', require('./accounts'));
router.use('/teachers-requests', require('./teachers-requests'));
router.use('/schools-requests', require('./schools-requests'));
module.exports = router;
