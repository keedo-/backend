let express = require('express');
let router = express.Router();
let debug = require('debug')('routes:api:v0:admin:accounts');

let Account = require("../../../../models/account");
let emailJob = require('../../../../jobs/emails');
/**
 * Create brands for a business account
 */
/**
 * Get list of brands based on geo
 */
router.get('/', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	Account
			.find(
					{
						"email":
								{$regex: new RegExp("^" + search.toLowerCase(), "i")}
					})
			.select('email accountType name')
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				console.log(err);
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send({ok: true, data: results});
			});
});
/**
 *
 */
router.post('/set-role', function (req, res) {
	let email_account = req.body.email;
	let role = req.body.role;
	// rol should be in ['TEACHER', 'STUDENT']
	Account.find({email: email_account}, function (err, found) {
		if (err) {
			res.send({ok: false, data: []})
		}
		if (found) {
			// have a different role and the user is not the current user
			if (found.accountType !== role && found.id !== req.user.id) {
				found.accountType = role;
				found.save((err, result) => {
					if (err) {
						res.send({ok: false, error: err})
					}
					if (result) {
						res.send({ok: true});
						emailJob.push()
					}
				})
			}
		}
	})
});
module.exports = router;
