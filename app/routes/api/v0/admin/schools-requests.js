let express = require('express');
let router = express.Router();
let uuid = require('uuid');

let debug = require('debug')('routes:api:v0:admin:schoolRequest');

let SchoolRequest = require("../../../../models/schoolRequest");
let Account = require("../../../../models/account");


/**
 * Create product for a business account
 */
router.post('/', function (req, res) {

});

/**
 * Create product for a business account
 */
router.post('/:id', function (req, res) {

});


function find(idCreatedBy, req, res) {
	let {page, limit, q} = req.query;
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;
	let findQuery = {};
	if (idCreatedBy) {
		findQuery = {createdBy: idCreatedBy}
	}
	SchoolRequest
			.find(findQuery)
			.populate('createdBy', 'email')
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send({ok: true, data: results});
			});
}

/**
 * Get list of brands based on geo
 */
router.get('/', function (req, res) {
	if (req.query.email) {
		Account.findOne({email: req.query.email}, (err, found) => {
			if (err) {
				res.send({ok: false, data: []})
			}
			if (found) {
				find(found.id, req, res);
			} else {
				res.send({ok: true, data: []})
			}
		})
	} else {
		find(null, req, res);
	}
});
module.exports = router;
