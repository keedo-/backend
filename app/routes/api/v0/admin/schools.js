let express = require('express');
let router = express.Router();
let uuid = require('uuid');

let debug = require('debug')('routes:api:v0:admin:schools');

let School = require("../../../../models/school");

/**
 * Create brands for a business account
 */

/**
 * Create product for a business account
 */
router.post('/', function (req, res) {
	let entry = new School();
	entry.name = req.body.name;
	entry.description = req.body.description;
	entry.address = req.body.address;
	entry.location = {type: 'Point', coordinates: [req.body.lat, req.body.lng]};


	// uuid and owner
	entry.createdBy = req.user.id;
	entry.uuid = uuid.v1();

	entry.save(function (err, product) {
		if (err) {
			return res.status(500).send({ok: false, reason: err});
		}
		if (product) {
			return res.send({ok: true, entry: entry});
		} else {
			return res.send({ok: false, product: null});
		}
	});
});


/**
 * Get list of brands based on geo
 */
router.get('/', function (req, res) {
	let {page, limit, q} = req.query;
	let search = q || '';
	let skip = ((page || 1) - 1) * 10;
	limit = parseInt(limit) || 10;

	School
			.find(
					{
						"name":
								{$regex: new RegExp("^" + search.toLowerCase(), "i")}
					})
			.limit(limit)
			.skip(skip)
			.exec(function (err, results) {
				console.log(err);
				if (err) {
					return res.send({ok: false, reason: 'Server'})
				}
				res.send({ok: true, data: results});
			});
});
module.exports = router;
