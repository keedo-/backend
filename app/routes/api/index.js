let express = require('express');
let router = express.Router();
let debug = require('debug')('app:route:index');

let Account = require('../../models/account');

/**
 *
 * @param req
 * @param res
 * @param next
 */
function loggedIn(req, res, next) {
	if (req.user) {
		next();
	} else {
		if (req.method === 'OPTIONS') {
			return res.send('ok');
		}
		res.status(401).send({status: 401, reason: "not_authorized"});
	}
}

/**
 *
 * @param req
 * @param res
 * @param next
 */
function isAdmin(req, res, next) {
	if (req.user) {
		// TODO, is ADMIN
		// CHANGE THIS
		Account.findOne(
				{accountType: 'USER', _id: req.user.id},
				(err, entry) => {
					if (err || !entry) {
						return res.status(401).send({status: 401, reason: "not_authorized"});
					}
					next()
				});
	} else {
		if (req.method === 'OPTIONS') {
			return res.send('ok');
		}
		res.status(401).send({status: 401, reason: "not_authorized"});
	}
}

/**
 *
 * @param passport
 * @returns {*}
 */
module.exports = function (passport) {
	debug("receiving passport");
	router.use('/accounts', require('./accounts')(passport));
	router.use('/teacher', loggedIn, require('./v0/teacher/index'));
	router.use('/admin', loggedIn, isAdmin, require('./v0/admin/index'));
	router.use(loggedIn, require('./v0/public'));
	return router;
};
