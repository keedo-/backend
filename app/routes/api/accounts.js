let express = require('express');
let uuid = require('uuid');
let path = require('path');
let router = express.Router();
let debug = require('debug')('app:routes:account');

let accountsEmails = require('../../emails/accounts');

let randomBytes = require('crypto').randomBytes;


let config = require('../../../config/global.js');
let Account = require('../../models/account');

let emailJob = require('../../jobs/emails');
let helpers = require('../../utils/helpers');


//

function sendResetPassword(account, token) {
	let locale = "fr";
	let resetPasswordLink = [config[config.env].host, 'accounts', 'reset-password', token].join('/');
	accountsEmails.sendResetPassword(
			locale,
			{link: resetPasswordLink, email: account.email}, (err, data) => {
				emailJob.push(data);
			}
	);
}

/**
 *
 * @param passport
 * @returns {*}
 */
module.exports = function (passport) {

	/* Handle Login POST */
	router.post('/login', function (req, res) {
		passport.authenticate('login', function (err, user) {
			if (err) {
				return res.status(err.status).send(err);
			}
			// req / res held in closure
			req.logIn(user, function (err) {
				debug("Log user in ");
				if (err) {
					return res.status(500).send({ok: false, reason: 'Please check your credential'});
				}
				let data = user.toJSON();
				delete data['password'];
				delete data['activationToken'];
				delete data['activationTokenExpires'];
				delete data['isActive'];
				delete data['isVerified'];
				delete data['__v'];
				randomBytes(48, function (err, buffer) {
					let token = buffer.toString('hex');
					let x = 1; //or whatever offset
					let expires = new Date();
					expires.setMonth(expires.getMonth() + x);
					let tokens = user.accessTokens;
					tokens = tokens.sort((a, b) => b.created - a.created);
					tokens = tokens.slice(0, 4);
					tokens.push({token: token, expires: expires, created: Date.now()});
					user.accessTokens = tokens;
					user.save(function () {
						return res.send({ok: true, user: data});
					});
				});
			});
		})(req, res);
	});

	/* Handle Login POST */
	router.post('/logout', function (req, res) {
		req.logout();
		return res.send({ok: true})
	});
	/* Handle Registration POST */
	router.post('/sign-up', function (req, res) {
				passport.authenticate('signup', function (err, user) {
					if (err) {
						return res.status(err.status).send(err);
					}
					// req / res held in closure
					req.logIn(user, function (err) {
						if (err) {
							return res.status(500).send({ok: false});
						}
						return res.send({ok: true});
					});

				})(req, res)
			}
	);
	/* Handle Registration POST */
	router.post('/forgot-password', function (req, res) {
		Account.findOne({'email': req.body.email},
				function (err, user) {
					// In case of any error, return using the done method
					if (err)
						return res.status(500).send({ok: false, reason: 'server'});
					// Username does not exist, log the error and redirect back
					if (!user) {
						return res.status(404).send({ok: false, reason: 'no_user'});
					}
					user.passwordResetToken = uuid.v4();
					let date = new Date();
					date.setDate(date.getDate() + 1);
					user.passwordExpiresAt = date;
					user.save((err, saveResult) => {
						if (err) {
							return res.status(500).send({ok: false, reason: 'server'});
						}
						sendResetPassword(user, user.passwordResetToken);
						return res.send({ok: true})
					})

				}
		);
	});
	/* Handle Registration POST */
	router.post('/reset-password', function (req, res) {
		let body = req.body;
		if (!body.token) {
			return res.status(400).send({ok: false, reason: 'server'});
		}
		Account.findOne({passwordResetToken: body.token},
				function (err, user) {
					// In case of any error, return using the done method
					if (err)
						return res.status(500).send({ok: false, reason: 'server'});
					// Username does not exist, log the error and redirect back
					if (!user) {
						return res.status(404).send({ok: false, reason: 'no_user'});
					}
					if (body.password === body.confirmPassword) {
						if (user.passwordExpiresAt > new Date()) {
							user.passwordResetToken = null;
							user.passwordExpiresAt = null;
							user.password = helpers.createPasswordHashWithSalt(body.password);
							user.save((err, _res) => {
								if (err) {
									return res.status(500).send({ok: false, reason: 'server'});
								}
								return res.send({ok: true})
							})
						} else {
							return res.status(400).send({ok: false, reason: 'token expired'});
						}
					} else {
						return res.status(400).send({ok: false, reason: 'password not confirmed'});
					}
				}
		);
	});


	return router;
};
