let express = require('express');
let router = express.Router();
let debug = require('debug')('app:route:index');
router.use('/accounts', require('./accounts'));
module.exports = router;
