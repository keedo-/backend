let express = require('express');
let router = express.Router();
let debug = require('debug')('app:routes:accounts');

router.get('/login', function (req, res) {
	res.render('accounts/login');
});
router.get('/forgot-password', function (req, res) {
	res.render('accounts/forgot_password');
});
router.get('/reset-password', function (req, res) {
	const token = req.params['token'];
	res.render('accounts/reset_password', {token: token});
});
router.get('/register', function (req, res) {
	res.render('accounts/register');
});
module.exports = router;
