let mongoose = require('mongoose');

let Schema = mongoose.Schema;
let requestTeacherSchema = new mongoose.Schema({
	school: {type: Schema.Types.ObjectId, ref: 'schools', required: true},
	teacher: {type: Schema.Types.ObjectId, ref: 'accounts', required: true},
	state: {type: String, required: true, default: 'WAITING'},
	createdAt: {type: Date, default: Date.now},
	updatedAt: {type: Date, default: Date.now}
});

let teacherRequest = mongoose.model('requests_teacher', requestTeacherSchema);
module.exports = teacherRequest;
