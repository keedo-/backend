let mongoose = require('mongoose');
let validator = require('validator');

let Schema = mongoose.Schema;

const PointSchema = require('./pointSchema');

let isZipCode = (zipCode) => {
	return validator.isPostalCode(String(zipCode), "any");
};
let SchoolSchema = new Schema({
			name: {type: String, required: true, unique: true},
			createdBy: {type: Schema.Types.ObjectId, ref: 'accounts'},
			director: {type: Schema.Types.ObjectId, ref: 'accounts'},
			category: {type: Schema.Types.ObjectId, ref: 'categories'},
			teachers: [{type: Schema.Types.ObjectId, ref: 'accounts'}],
			uuid: {type: String, required: true, unique: true},
			description: {type: String, required: false},
			postalCode: {type: Number, required: false, validate: [isZipCode, 'invalid zip code']},
			city: {type: String, required: false},
			address: {type: String, required: false},
			location: {
				type: PointSchema,
				required: false
			},
			createdSince: {type: Number},
			schoolPicture: {type: String},
			schoolPictureThumb: {type: String},
			tags: Array,
			createdAt: {type: Date, default: Date.now},
			updatedAt: {type: Date, default: Date.now},
		})
;
let School = mongoose.model('schools', SchoolSchema);

module.exports = School;
