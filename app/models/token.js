let validator = require('validator');
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let tokenSchema = new mongoose.Schema({
    token: {type: String, required: true, unique: true},
    provider: {type: String, required: true},
    account: {type: Schema.Types.ObjectId, ref: 'accounts', required: false},
    info: {type: Object, required: false},
    location: {
        type: {type: String},
        coordinates: [Number],
        full: {type: Object},
        required: false
    }
});

let Token = mongoose.model('tokens', tokenSchema);
module.exports = Token;