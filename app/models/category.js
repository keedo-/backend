let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let categorySchema = new Schema({
        name: {type: String, required: true, index: {unique: true}, lowercase: true},
        name_ar: {type: String, required: true},
        name_fr: {type: String, required: true},
        description: {type: String, required: false},
        description_ar: {type: String, required: false},
        description_fr: {type: String, required: false},
        icon: {type: String, required: false},
    })
;
let Category = mongoose.model('categories', categorySchema);

module.exports = Category;