let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let chapterSchema = new Schema(
		{
			class: {type: Schema.Types.ObjectId, ref: 'classes', required: true},
			teacher: [{type: Schema.Types.ObjectId, ref: 'accounts', required: true}],
			name: {type: String, required: true},
			description: {type: String},
			details: Object,
			createdAt: {type: Date, default: Date.now},
			updatedAt: {type: Date, default: Date.now},
		}
);

let ClassChapter = mongoose.model('class_chapters', chapterSchema);

module.exports = ClassChapter;
