let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let brandSchema = new Schema(
		{
			teacher: {type: Schema.Types.ObjectId, ref: 'accounts', required: true},
			school: {type: Schema.Types.ObjectId, ref: 'schools', required: false},
			students: [{type: Schema.Types.ObjectId, ref: 'accounts'}],
			name: {type: String, required: true},
			description: {type: String},
			launchedSince: {
				type: Date
			},
			details: Object,
			createdAt: {type: Date, default: Date.now},
			updatedAt: {type: Date, default: Date.now},
		}
);
brandSchema.index({name: 1, teacher: 1}, {unique: true});

let Class = mongoose.model('classes', brandSchema);

module.exports = Class;
