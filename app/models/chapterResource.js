let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let chapterResourceSchema = new Schema({
			chapter: {type: Schema.Types.ObjectId, ref: 'class_chapters', required: true},
			filename: {type: String, required: true},
			default_version: {
				type: String, default: null
			},
			location: String,
			storageLocation: String,
			description: {type: String},
			versions: [Object],
			details: Object,
			createdAt: {type: Date, default: Date.now},
			updatedAt: {type: Date, default: Date.now},
		})
;

let chapterResource = mongoose.model('chapter_resources', chapterResourceSchema);

module.exports = chapterResource;
