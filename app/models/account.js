let validator = require('validator');
let mongoose = require('mongoose');

let isZipCode = (zipCode) => {
	return validator.isPostalCode(String(zipCode), "any");
};
let brandSchema = new mongoose.Schema({
	username: {type: String, required: true},
	firstName: {type: String, required: false},
	lastName: {type: String, required: false},
	password: {type: String, required: true},
	email: {type: String, required: true, validate: [validator.isEmail, 'invalid email']},
	address: {type: String, required: false},
	phoneNumber: {type: String, required: false},
	lastLocation: {type: String, required: false},
	businessName: {type: String, required: false},
	country: {type: String, required: false},
	city: {type: String, required: false},
	zipCode: {type: Number, required: false, validate: [isZipCode, 'invalid zip code']},
	passwordResetToken: {type: String},
	passwordExpiresAt: {type: Date},
	isActive: {type: Boolean, default: false},
	activationToken: {type: String, default: false},
	activationTokenExpires: {type: Date},
	isVerified: {type: Boolean, default: false},
	accountType: {type: String, default: 'USER'},
	accessTokens: {type: Array},
	profile: {type: JSON},
});

let User = mongoose.model('accounts', brandSchema);
module.exports = User;
