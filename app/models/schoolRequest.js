let mongoose = require('mongoose');

let Schema = mongoose.Schema;
let schoolRequestSchema = new mongoose.Schema({
	createdBy: {type: Schema.Types.ObjectId, ref: 'accounts', required: true},
	school: {type: String, required: true},
	address: {type: String, required: true},
	state: {type: String, required: true, default: 'WAITING'},
	createdAt: {type: Date, default: Date.now},
	updatedAt: {type: Date, default: Date.now}
});

let schoolRequest = mongoose.model('schools_requests', schoolRequestSchema);
module.exports = schoolRequest;
