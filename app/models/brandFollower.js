let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let brandFollowerSchema = new Schema({
        brand: {type: Schema.Types.ObjectId, ref: 'brands', required: true},
        followers: [{type: Schema.Types.ObjectId, ref: 'accounts'}],
    })
;
let Brand = mongoose.model('brands_followers', brandFollowerSchema);

module.exports = Brand;