let baseApi = '/api/';

// Add a 401 response interceptor
axios.interceptors.response.use(function (response) {
	return response;
}, function (error) {
	if (401 === error.response.status) {
		// here block it
		window.location = `/login?redirect=${window.location.pathname}`
	} else {
		return Promise.reject(error);
	}
});

const API = {
	getBaseApi() {
		return baseApi;
	},
	request(method, uri, data = null, options = null) {
		if (!method) {
			return
		}

		if (!uri) {
			return
		}
		options = options || {};

		let url = baseApi + uri;
		let config = {method: method, url, data, withCredentials: true};
		if (options.headers) {
			config['headers'] = options.headers;
		}
		return axios(config);
	}
};
// 
API.accounts = {
	login(data = null) {
		return API.request('post', 'accounts/login', data)
	},
	register(data = null) {
		return API.request('post', 'accounts/sign-up', data)
	},
	forgotPassword(data = null) {
		return API.request('post', 'accounts/forgot-password', data)
	},
	resetPassword(data = null) {
		return API.request('post', 'accounts/reset-password', data)
	}
};
