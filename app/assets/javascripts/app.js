App = {
	Account: {},
	i18n: function () {
	}
};
// Login
App.Account.Login = {
	isLoading: false,
	submit: function (event) {
		event = event || window.event;
		event.stopImmediatePropagation();
		event.preventDefault();
		App.Account.Login.toggleLoading();
		var login = $('input[name="login"]').val();
		var password = $('input[name="password"]').val();
		/* Making API call to authenticate a user */
		API.accounts
				.login({login: login, password: password})
				.then(response => {
					App.Account.Login.toggleLoading();
					let data = response.data;
					/* Checking if error object was returned from the server */
					if (!data.ok) {
						App.Account.Login.displayError(data.reason);
					} else {
						window.location = `/home`;
					}
				})
				.catch(error => {
					App.Account.Login.displayError(error.response.data.reason);
					App.Account.Login.toggleLoading()
				});
		return false;
	},
	displayError(text) {
		$("#error").html(text);
	},
	toggleLoading: function () {
		App.Account.Login.isLoading = !App.Account.Login.isLoading;
		$('button[type="submit"]').text(App.Account.Login.isLoading ? 'loading ..' : 'Se connecter');
	}
};

// ForgotPassword
App.Account.ForgotPassword = {
	isLoading: false,
	submit: function (event) {
		event = event || window.event;
		event.stopImmediatePropagation();
		event.preventDefault();
		App.Account.ForgotPassword.toggleLoading();
		var email = $('input[name="email"]').val();
		/* Making API call to authenticate a user */
		API.accounts
				.forgotPassword({email: email})
				.then(response => {
					App.Account.ForgotPassword.toggleLoading();
					let data = response.data;
					/* Checking if error object was returned from the server */
					if (!data.ok) {
						App.Account.ForgotPassword.displayError(data.reason);
					} else {
						App.Account.ForgotPassword.displaySuccess();
					}
				})
				.catch(error => {
					App.Account.ForgotPassword.displayError(error.response.data.reason);
					App.Account.ForgotPassword.toggleLoading()
				});
		return false;
	},
	displayError(text) {
		$("#error").html(text);
	},
	toggleLoading: function () {
		App.Account.ForgotPassword.isLoading = !App.Account.ForgotPassword.isLoading;
		$('button[type="submit"]').text(App.Account.ForgotPassword.isLoading ? 'loading ..' : 'Se connecter');
	},
	displaySuccess() {
		App.Account.ForgotPassword.isLoading = false ;

	}
};




// ForgotPassword
App.Account.ResetPassword = {
	isLoading: false,
	submit: function (event) {
		event = event || window.event;
		event.stopImmediatePropagation();
		event.preventDefault();
		App.Account.ResetPassword.toggleLoading();
		var password = $('input[name="password"]').val();
		var confirmPassword = $('input[name="confirmPassword"]').val();
		var token = $('input[name="token"]').val();
		/* Making API call to authenticate a user */
		API.accounts
				.forgotPassword({password: password,  confirmPassword : confirmPassword, })
				.then(response => {
					App.Account.ResetPassword.toggleLoading();
					let data = response.data;
					/* Checking if error object was returned from the server */
					if (!data.ok) {
						App.Account.ResetPassword.displayError(data.reason);
					} else {
						App.Account.ResetPassword.displaySuccess();
					}
				})
				.catch(error => {
					App.Account.ResetPassword.displayError(error.response.data.reason);
					App.Account.ResetPassword.toggleLoading()
				});
		return false;
	},
	displayError(text) {
		$("#error").html(text);
	},
	toggleLoading: function () {
		App.Account.ResetPassword.isLoading = !App.Account.ResetPassword.isLoading;
		$('button[type="submit"]').text(App.Account.ResetPassword.isLoading ? 'loading ..' : 'Se connecter');
	},
	displaySuccess() {
		App.Account.ResetPassword.isLoading = false ;

	}
};
