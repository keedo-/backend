const path = require('path');

const builder = require('./_builder');
const i18n = require('./../i18n');
module.exports = {
	/**
	 *
	 * @param locale
	 * @param data
	 * @param cb
	 * @returns {{subject: *, html: builder, to: (email|{$regex}|*|string|{index, type, required}|{type, required, validate})}}
	 */
	buildActivationAccountEmail: (locale, data, cb) => {
		let template = path.join('accounts', 'activation_link.' + (locale || 'fr'));
		const subject = i18n.t(locale, "emails.accounts.activation_subject");
		builder({template, data}, (err, html) => {
			if (err) {
				return cb(err)
			}
			cb(null, {
				to: data.email,
				html: html,
				subject: subject
			});
		})
	}, /**
	 *
	 * @param locale
	 * @param data
	 * @param cb
	 * @returns {{subject: *, html: builder, to: (email|{$regex}|*|string|{index, type, required}|{type, required, validate})}}
	 */
	sendRoleModification: (locale, data, cb) => {
		let template = path.join('accounts', 'role.' + (locale || 'fr'));
		const subject = i18n.t(locale, "emails.accounts.role_change");
		builder({template, data}, (err, html) => {
			if (err) {
				return cb(err)
			}
			cb(null, {
				to: data.email,
				html: html,
				// FIXME , use locale
				subject: subject
			});
		})
	},
	/**
	 *
	 * @param locale
	 * @param data
	 * @param cb
	 * @returns {{subject: *, html: builder, to: (email|{$regex}|*|string|{index, type, required}|{type, required, validate})}}
	 */
	sendResetPassword: (locale, data, cb) => {
		let template = path.join('accounts', 'forgot_password.' + (locale || 'fr'));
		const subject = i18n.t(locale, "emails.accounts.reset_password");
		builder({template, data}, (err, html) => {
			if (err) {
				return cb(err)
			}
			cb(null, {
				to: data.email,
				html: html,
				// FIXME , use locale
				subject: subject
			});
		})
	},
};
