const path = require('path');

const builder = require('./_builder');
const i18n = require('./../i18n');

let emailJob = require('../jobs/emails');

const EMAIL_FOLDER = 'students';
module.exports = {
	sendInvitationToClass: (locale, data, cb) => {
		let template = path.join(EMAIL_FOLDER, 'invite.' + (locale || 'fr'));
		let email = data.email;
		let buildArgs = Object.assign({}, {data: data}, {template: template});
		const subject = i18n.t(locale, "students.classes.invitation");

		builder(buildArgs, (err, html) => {
			if (err) {
				return cb(err)
			}
			const emailData = {
				to: email,
				html: html,
				// FIXME , use locale
				subject: subject
			};
			if (cb) {
				cb(null, emailData);
			} else {
				emailJob.push(emailData);
			}
		})
	},
};
