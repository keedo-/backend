const path = require("path");
const fs = require('fs');
const pug = require("pug");
/**
 *
 * @param options
 * @param cb Function
 */
const builder = function (options, cb) {
	// path to template
	let data = options.data || {};
	data.year = new Date().getFullYear();
	let pathToFile = path.join(path.join(process.cwd(), 'app', 'views', 'emails', options.template + '.pug'));
	fs.readFile(pathToFile, 'utf8', (err, content) => {
		if (content) {
			const fnTemplate = pug.compile(content, data);
			return cb(null, fnTemplate(data));
		}
		return cb(err);
	});
};
module.exports = builder;
