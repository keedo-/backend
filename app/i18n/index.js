// Ready translated locale messages
locales = {
	en: require('./en'),
	fr: require('./fr')
};
module.exports = {
	t(locale, key) {
		return locales[locale][key];
	}
};
