let bCrypt = require('bcrypt-nodejs');


let config = require('../../config/global');

module.exports = {
    // Generates hash using bCrypt
    createPasswordHashWithSalt: function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    },
    /**
     *
     * @param user
     * @param password
     * @returns {*}
     */
    isValidPassword: function (user, password) {
        return bCrypt.compareSync(password, user.password);
    },
    /**
     *
     */
    getUploadFolder: function () {
        return config[config.env].uploadFolder;
    },

    /**
     *
     */
    getAssetsHost: function () {
        return config[config.env].assetsHost;
    }
};
