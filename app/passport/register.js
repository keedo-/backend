let path = require('path');
let debug = require('debug')('account:register:register');
let LocalStrategy = require('passport-local').Strategy;
let uuid = require('uuid');


let Account = require('../models/account');
let accountMailers = require('../emails/accounts');
/*
Account.remove({}, (err)=> {
    debug('removed');
});*/
let config = require('../../config/global.js');


let emailJob = require('../jobs/emails');
let helpers = require('../utils/helpers');

/**
 *
 * @param locale
 * @param account
 * @param activationToken
 */
function sendActivationAccountLink(locale, account, activationToken) {
	let link = [config[config.env].host, 'accounts', 'verify', activationToken].join('/');
	accountMailers.buildActivationAccountEmail(locale,
			{link: link, email: account.email, username: account.username}, (err, data) => {
				emailJob.push(data);
			}
	);
}

module.exports = function (passport) {
	passport.use('signup', new LocalStrategy({
				usernameField: 'email',
				passwordField: 'password',
				session: false,
				passReqToCallback: true
			},
			function (req, username, password, done) {
				findOrCreateAccount = function () {
					// find a account in Mongo with provided accountname
					Account.findOne({'email': username}, function (err, account) {
						// In case of any error, return using the done method
						if (err) {
							return done({ok: false, reason: 'server', status: 500});
						}
						// already exists
						if (account) {
							return done({
								ok: false,
								response: 'already_exists',
								status: 400
							}, false, req.flash('message', 'Account Already Exists'));
						} else {
							// if there is no account with that email
							// create the account

							let body = req.body;
							let newAccount = new Account();
							newAccount.password = helpers.createPasswordHashWithSalt(password);
							newAccount.email = body['email'];
							newAccount.username = body['username'] || [body['lastName'], body['lastName']].join('_');
							newAccount.lastName = body['lastName'];
							newAccount.firstName = body['firstName'];
							//newAccount.zipCode = body['zipCode'];
							//	newAccount.city = body['city'];
							//	newAccount.country = body['country'];
							//	newAccount.address = body['address'];
							//	newAccount.businessName = body['businessName'];
							//	newAccount.phoneNumber = body['phoneNumber'];

							let expires = new Date();
							expires.setDate(expires.getDate() + 30);
							newAccount.activationToken = uuid.v1();
							newAccount.activationTokenExpires = expires;

							// save the account
							newAccount.save(function (err, entry) {
								if (err) {
									//console.log('Error in Saving account: ' + err);
									return done({reason: err, status: 400}, null);
								}
								//console.log('Account Registration successful');
								sendActivationAccountLink(req.locale || 'fr', newAccount, newAccount.activationToken);
								return done(null, newAccount);
							});
						}
					});
				};
				// Delay the execution of findOrCreateAccount and execute the method
				// in the next tick of the event loop
				process.nextTick(findOrCreateAccount);
			})
	);


};
