let LocalStrategy = require('passport-local').Strategy;
let debug = require('debug')('app:passport:login');
let Account = require('../models/account');


let helpers = require('../utils/helpers');

module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function (email, password, done) {
            // Check in mongo if a user with username exists or not
            Account.findOne({'email': email}, function (err, user) {
                    debug('#######################');
                    // In case of any error, return using the done method
                    if (err)
                        return done({ok: false, reason: 'server', status: 500});
                    // Username does not exist, log the error and redirect back
                    if (!user) {
                        debug('#######################');
                        return done({ok: false, reason: 'not_found', status: 404});
                    }
                    // User exists but wrong password, log the error
                    if (!helpers.isValidPassword(user, password)) {
                        return done({ok: false, reason: 'invalid_credential', status: 400}); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success

                    return done(null, user);
                }
            ).catch(function (err, res) {
                return done({ok: false, reason: 'server', status: 500}, user);
            });
        })
    );
};