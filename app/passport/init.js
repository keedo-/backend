let passport = require('passport');
let registerLogin = require('./login');
let registerSignUp = require('./register');
let debug = require('debug')('passport:init');

let Account = require('../models/account');

let log = require('../utils/log');

// Passport needs to be able to serialize and deserialize users to support persistent login sessions
passport.serializeUser(function (user, done) {
	done(null, user._id);
});

passport.deserializeUser(function (id, done) {
	Account.findById(id, function (err, user) {
		let currUser = {};
		currUser._id = user.id;
		currUser.id = user.id;
		currUser.accountType = user.accountType;
		currUser.isVerified = user.isVerified;
		currUser.profile = user.profile;
		currUser.firstName = user.firstName;
		currUser.lastName = user.lastName;
		//choose which fields to keep in req.user
		currUser.email = user.email;
		currUser.name = user.name;
		//currUser is what is saved in 'req.user'
		done(err, currUser);
	});
});

// Setting up Passport Strategies for Login and SignUp/Registration

debug('initializing passport');
registerLogin(passport);
registerSignUp(passport);

module.exports = passport;
