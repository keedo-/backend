let queue = require('./queue').EmailQueue;
let nodeMailer = require('nodeMailer');
let debug = require('debug')('jobs:email');

const nodeMailerTransport = nodeMailer.createTransport(process.env.KEEDO_SMTP_URI);
// verify connection configuration
nodeMailerTransport.verify(function (error, success) {
	if (error) {
		debug(error);
	} else {
		debug("Server is ready to take our messages");
	}
});
queue.process(function (job, done) {
	let data = job.data;
	const message = {
		to: data.to,
		from: data.from || "no-reply@keedo.fr",
		subject: data.subject,
		html: data.html,
		text: data.text
	};
	nodeMailerTransport.sendMail(message).then(info => {
		done(null)
	}).catch((err) => {
		done(err);
	});
});

/**
 *
 * @type {{sendMail: (function(*=))}}
 */
module.exports = {
	push(args) {
		queue.add(args);
	}
};
