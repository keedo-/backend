let queue = require('./queue');
let admin = require('firebase-admin');
let debug = require('debug')('jobs:fcm');


let FCM_JOB = "FCM";
// 30 push sending operation
queue.process(FCM_JOB, 30, function (job, done) {
    let data = job.data;
    //
    let dryRun = true;
    admin.messaging().send(data, dryRun)
        .then((response) => {
            // Response is a message ID string.
            debug('Dry run successful:', response);
        })
        .catch((error) => {
            debug('Error during dry run:', error);
        });
});
/**
 *
 * @type {{sendMail: (function(*=))}}
 */
module.exports = {
    sendPush(options) {
        queue.create(FCM_JOB, options).save(function (err, result) {
            if (err) {
                debug("Error with creating job : fcm ");
                debug(err);
                return;
            }
            debug("Success sending mail")
        });
    }
};