let queue = require('./queue');
const sgMail = require('@sendgrid/mail');
let debug = require('debug')('jobs:email');


let EMAIL_JOB = "email";

sgMail.setApiKey(process.env.KEEDO_SENDGRID_API_KEY);
// process 10 emails at a time

queue.process(EMAIL_JOB, 10, function (job, done) {
    let data = job.data ;
    const msg = {
        to: data.to,
        from: "no-reply@keedo.fr",
        subject: data.subject,
        html: data.html,
    };
    sgMail.send(msg, (err, result) => {
        if (err) {
            return done(new Error(err));
        }
        done(null)
    });
});
/**
 *
 * @type {{sendMail: (function(*=))}}
 */
module.exports = {
    sendMail(options) {
        queue.create(EMAIL_JOB, options).save(function (err, result) {
            if (err) {
                debug("Error with creating job ");
                debug(err);
                return;
            }
            debug("Success sending mail")
        });
    }
};
