var Queue = require('bull');
var EmailQueue = new Queue('emails', process.env.KEEDO_REDIS_HOST);

module.exports = {
	EmailQueue: EmailQueue
};
