let configDB = require('./../../config/database');
let Category = require('./../../app/models/category');
let mongoose = require('mongoose');
console.log(configDB[configDB.env].dbUrl);
// configuration ===============================================================
mongoose.connect(configDB[configDB.env].dbUrl, {
    autoReconnect: true,
    socketTimeoutMS: 6000000
}).catch(err => { // mongoose connection error will be handled here
    console.error('App starting error:', err.stack);
    process.exit(1);
}).then(function () {
    let categories = require('./categories.json');
    Category.create(categories, function (err, results) {
        if (err){
            // ...
            console.log(err);
        }
    });
}); // connect to our database
