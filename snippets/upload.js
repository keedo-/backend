
/**
 * Create brands for a business account
 */
router.post('/', function (req, res) {
	let dataForm = {};

	/**
	 *
	 * @param brand
	 * @param response
	 * @returns {Promise<[ClientResponse , {}]> | * | void | boolean}
	 * @private
	 */
	function _uploadOrReply(brand, response) {
		if (!response.ok) {
			return _catchHandler(response);
		}
		return res.send({ok: true, brand: brand.toJSON()})
	}

	/**
	 *
	 * @param err
	 * @returns {Promise<[ClientResponse , {}]> | * | void | boolean}
	 * @private
	 */
	function _catchHandler(err) {
		debug("_catchHandler");
		debug(err);
		return res.status(500).send({reason: JSON.stringify(err), ok: false})
	}


	function _updateOrCreate() {
		// check if already exist
		Schools.findOne({owner: req.user._id}, function (err, brand) {
			if (err) {
				return res.status(500).send({ok: false, reason: 'server'});
			}
			debug(dataForm);
			if (!brand) {
				debug("creating new");
				debug(req.user);
				delete dataForm['_id'];
				let brandData = Object.assign(dataForm, {
					owner: req.user._id,
				}, {});
				let brand = new Schools(brandData);
				brand.save().then(_uploadOrReply).catch(_catchHandler);
			} else {
				dataForm._id = brand._id;
				brand.update(dataForm).then(_uploadOrReply.bind(this, brand)).catch(_catchHandler);
			}
		});
	}

	let busboy = new Busboy({headers: req.headers});
	busboy.on('field', function (fieldname, val) {
		debug(fieldname);
		debug(val);
		dataForm[fieldname] = val;
	});
	busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
		// if need upload
		let uploadFolder = helpers.getUploadFolder();
		let newFileName = uuid.v1();
		let saveTo = path.join(uploadFolder, newFileName);
		debug('Uploading File : ' + saveTo);
		file.pipe(fs.createWriteStream(saveTo));
		file.on('data', function (data) {
			log('uplodading', 'File [' + fieldname + '] got ' + data.length + ' bytes');
		});
		file.on('end', function () {
			debug("end downloading file");
			dataForm.brandPicture = newFileName;
		});
	});
	busboy.on('finish', function () {
		_updateOrCreate();
	});
	req.pipe(busboy);
})
;
